/**
 * nkn-ordered-multiclient
 *
 * nkn-multiclient but it sorts on startup.
 */
const Multiclient = require('nkn-multiclient/lib/multiclient')
const Queue = require('queue')

/**
 * TODO add close method which removes listeners?
 * TODO make suggestion of using `events` module instead of this home-baked event listener thing.
 */
class OrderedMulticlient extends Multiclient {
	/**
	 * multiclientOpts: options to forward to multiclient.
	 * opts: { startTimeout: 2000 // Time to wait for messages to sort -
	 *  after startup, before sending received events.
	 * object is passed as options to to queue.
	 * }
	 */
	constructor(multiclientOpts = {}, opts = {}) {
		super(Object.assign({
			numSubClients: 3,
			originalClient: false,
			msgCacheExpiration: 300 * 1000
		}, multiclientOpts))

		// Wait 2 seconds after connect before emitting any messages.
		this._startTimeout = opts.startTimeout || 2000

		this._messageQueue = []
		this._queue = new Queue({
			// Bug happens when set to 0.
			timeout: 1,
			...opts,
			concurrency: 1, // ?
		})

		this.eventListeners['ordered-message'] = []

		this._connected = false

		this.on('connect', () => this._onConnect())

		this.on('message', (...args) => this._onMessage(...args))
	}

	_emitMessage(args) {
		this.eventListeners['ordered-message'].forEach(f => f(...args))
	}

	on(e, func) {
		if (e === 'ordered-message') {
			this.eventListeners[e].push(func)
		} else {
			super.on(e, func)
		}
	}

	// Override to customize.
	// a and b are [src, payload, payloadType, encrypt].
	// Payload has been parsed.
	sort(a, b) {
		if (!a[1].timestamp && !b[1].timestamp) {
			return 0
		} else if (!a[1].timestamp) {
			return 1
		} else if (!b[1].timestamp) {
			return -1
		}

		const dateA = new Date(a[1].timestamp)
		const dateB = new Date(b[1].timestamp)
		if (dateA > dateB) {
			return 1
		} else if (dateA < dateB) {
			return -1
		} else {
			return 0
		}
	}
	// Override to customize. args is [src, payload, payloadType, encrypt].
	parse(args) {
		try {
			args[1] = JSON.parse(args[1])
		} catch(e) {
			console.error('NKN message parsing error:', e, args)
		}
		return args
	}

	_sort() {
		this._messageQueue.sort(this.sort)
	}

	_onConnect() {
		this._connected = false
		setTimeout(() => {
			this._sort()
			this._messageQueue.forEach(i =>
				this._queue.push(() => this._emitMessage(i))
			)
			this._connected = true
			this._messageQueue = []
			this._queue.start()
		}, this._startTimeout)
	}

	_onMessage(...args) {
		args = this.parse(args)
		if (!this._connected) {
			this._messageQueue.push(args)
		} else {
			this._queue.push(() => this._emitMessage(args))
			this._queue.start()
		}
	}

}

module.exports = OrderedMulticlient
