# nkn-ordered-multiclient

An extension to [nkn-multiclient](https://www.npmjs.com/package/nkn-multiclient "nkn-multiclient on NPM") that will sort messages that come in rushing when you first connect to a node.

See [NKP-0017](https://forum.nkn.org/t/nkp-0017-cached-message-timelining/1923 "On nkn.org"), and the theory [(post #4)](https://forum.nkn.org/t/nkp-0017-cached-message-timelining/1923) that this aims to fix.

## Installation

```
$ npm i nkn-multiclient https://gitlab.com/losnappas/nkn-ordered-multiclient/
```

## Usage

### Simple usage

```javascript
import NKN from 'nkn-ordered-multiclient';

// Options are optional.
const nkn = new NKN(
	// multiclientOpts
	{
		numSubClients: 3,
		originalClient: false,
		msgCacheExpiration: 300 * 1000
	},
	{
		startTimeout: 2000,
		// ...opts then passed to queue; search npm for queue, if you care.
	}
);

// Note: message has already been parsed here.
nkn.on( 'ordered-message', ([src, message, payloadType, encrypt]) => console.log(message) );

// To not send ack-messages, just do as you would normally:
nkn.on( 'message', () => false );
```

### Advanced usage

```javascript
import NKN from 'nkn-ordered-multiclient';

class MyNKNWrapper extends NKN {
	// Constructor... Whatever you need...

	// Override the parser
	// args = [ src, payload, payloadType, encrypt ]
	parse( args ) {
		// Default:
		try {
			args[1] = JSON.parse(args[1])
			return args
		} catch(e) {
			return args
		}
	}
	
	// Override the sorter
	// a and b are each also [src, payload, payloadType, encrypt].
	sort( a, b ) {
		// By default, it has payload as JSON and now sorts by timestamp.
		if (!a[1].timestamp && !b[1].timestamp) {
			return 0
		} else if (!a[1].timestamp) {
			return 1
		} else if (!b[1].timestamp) {
			return -1
		}
		const dateA = new Date(a[1].timestamp)
		const dateB = new Date(b[1].timestamp)
		if (dateA > dateB) {
			return 1
		} else if (dateA < dateB) {
			return -1
		} else {
			return 0
		}
	}
}
```

## Example usage

[D-Chat nkn.js](https://gitlab.com/losnappas/d-chat/blob/master/src/workers/nkn/nkn.js), and [nknHandler.js](https://gitlab.com/losnappas/d-chat/blob/master/src/workers/nkn/nknHandler.js).
